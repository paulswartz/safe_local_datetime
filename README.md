# SafeLocalDatetime

Safely convert a NaiveDateTime into a zoned DateTime.

## Example

```elixir
iex> time_zone = "America/New_York"
iex> now = ~U[2021-11-07T06:59:00Z]
iex> {:ok, dt} = SafeLocalDateTime.new(~N[2021-11-07T01:59:57], time_zone, now, Tzdata.TimeZoneDatabase)
iex> dt
#DateTime<2021-11-07 01:59:57-04:00 EDT America/New_York>
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `safe_local_datetime` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:safe_local_datetime, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/safe_local_datetime](https://hexdocs.pm/safe_local_datetime).

