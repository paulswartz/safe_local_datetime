defmodule SafeLocalDateTimeTest do
  use ExUnit.Case
  use ExUnitProperties
  doctest SafeLocalDateTime

  describe "from_naive/3" do
    property "converts a date/time back even after an offset" do
      time_zone_db = Tzdata.TimeZoneDatabase

      possible_times =
        for time_zone <- Tzdata.zone_list(),
            utc_dt <- transition_times_for(time_zone) do
          {time_zone, utc_dt}
        end

      check all(
              {time_zone, utc_dt} <- member_of(possible_times),
              offset_sec <- member_of([0, -5, 5]),
              max_runs: 500
            ) do
        local_dt = DateTime.shift_zone!(utc_dt, time_zone, time_zone_db)
        naive_local_dt = DateTime.to_naive(local_dt)
        offset_utc_dt = DateTime.add(utc_dt, offset_sec, :second, time_zone_db)

        expected = local_dt

        {:ok, actual} =
          SafeLocalDateTime.from_naive(naive_local_dt, time_zone, offset_utc_dt, time_zone_db)

        assert DateTime.to_iso8601(expected) == DateTime.to_iso8601(actual)
      end
    end
  end

  # 20 years in the past
  @min_past_time DateTime.utc_now()
                 |> Map.update!(:year, &(&1 - 20))
                 |> DateTime.to_gregorian_seconds()
                 |> elem(0)
  # 20 years in the future
  @max_future_time DateTime.utc_now()
                   |> Map.update!(:year, &(&1 + 20))
                   |> DateTime.to_gregorian_seconds()
                   |> elem(0)

  defp transition_times_for(time_zone) do
    # pick out unix times when the given timezone is transitioning from one time zone to another
    {:ok, periods} = Tzdata.periods(time_zone)

    for period <- periods,
        %{utc: seconds} when is_integer(seconds) <- [period.from, period.until],
        seconds > @min_past_time,
        seconds < @max_future_time do
      DateTime.from_gregorian_seconds(seconds)
    end
  end
end
