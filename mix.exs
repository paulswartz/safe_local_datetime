defmodule SafeLocalDatetime.MixProject do
  use Mix.Project

  def project do
    [
      app: :safe_local_datetime,
      version: "0.1.0",
      elixir: "~> 1.11",
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.28", only: [:dev, :test], runtime: false},
      {:tzdata, "~> 1.1", only: :test},
      {:stream_data, "~> 0.5", only: :test},
      {:dialyxir, "~> 1.0", only: :dev, runtime: false}
    ]
  end
end
