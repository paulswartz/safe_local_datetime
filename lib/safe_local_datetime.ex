defmodule SafeLocalDateTime do
  @moduledoc """
  Safely convert a NaiveDateTime to a zoned DateTime.
  """

  @type error ::
          {:gap, DateTime.t(), DateTime.t()}
          | :incompatible_calendars
          | :time_zone_not_found
          | :utc_only_time_zone_database

  @doc """
  Safely convert a NaiveDateTime to a zoned DateTime.

  In order to disambiguate, it also takes an already zoned DateTime, and chooses
  the time closest to that.

  Some edge cases to consider:
  - sometimes the timezone name changes, but not the offset: this results in a different DateTime
  - timezone offsets can change by as a little as a single minute: your already zoned DateTime
    should be as close as possible to the NaiveDateTime
  - some local times don't convert cleanly to UTC (multiple DST transitions in a future year)

  ## Examples

      # before EDT -> EST transition
      iex> ndt = ~N[2021-11-07T01:59:57]
      iex> time_zone = "America/New_York"
      iex> now = ~U[2021-11-07T06:00:00Z]
      iex> {:ok, dt} = SafeLocalDateTime.from_naive(ndt, time_zone, now, Tzdata.TimeZoneDatabase)
      iex> dt
      #DateTime<2021-11-07 01:59:57-04:00 EDT America/New_York>

      # after EDT -> EST transition
      iex> ndt = ~N[2021-11-07T01:59:57]
      iex> time_zone = "America/New_York"
      iex> now = ~U[2021-11-07T07:00:00Z]
      iex> {:ok, dt} = SafeLocalDateTime.from_naive(ndt, time_zone, now, Tzdata.TimeZoneDatabase)
      iex> dt
      #DateTime<2021-11-07 01:59:57-05:00 EST America/New_York>

  """
  @spec from_naive(NaiveDateTime.t(), Calendar.time_zone(), DateTime.t()) ::
          {:ok, DateTime.t()} | {:error, error()}
  @spec from_naive(
          NaiveDateTime.t(),
          Calendar.time_zone(),
          DateTime.t(),
          Calendar.time_zone_database()
        ) ::
          {:ok, DateTime.t()} | {:error, error()}
  def from_naive(
        %NaiveDateTime{} = ndt,
        time_zone,
        %DateTime{} = now,
        time_zone_database \\ Calendar.get_time_zone_database()
      )
      when is_binary(time_zone) do
    case DateTime.from_naive(ndt, time_zone, time_zone_database) do
      {:ok, dt} ->
        {:ok, dt}

      {:gap, first, second} ->
        {:error, {:gap, first, second}}

      {:ambiguous, first, second} ->
        first_diff = DateTime.diff(first, now, :nanosecond)
        second_diff = DateTime.diff(second, now, :nanosecond)

        if abs(first_diff) <= abs(second_diff) do
          {:ok, first}
        else
          {:ok, second}
        end

      {:error, e} ->
        {:error, e}
    end
  end
end
